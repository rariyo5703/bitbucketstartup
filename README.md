
## Online Pastry Store

This project is an online store that enables a user to order their preferrd pastries online, generate their receipt and make payment. There is always an admin side that shows a list of all the orders made that is only accecible to the admin

---

## Building this project on your local machine

Following the bellow instructions to have a copy of this project.

1. Clone the project using git, sourcetree or vs code
2. Open your terminal and run "npm install" to retrieve all the node modules
3. Run the app using "node index.js" and this will prompt your browser open up the app 
4. You can stop running the app by clicking "control c" on a windows environment or "command c" on a mac environment 

---

##Licence
https://github.com/melawlah/PartsUnlimitedE2E/blob/master/LICENSE
