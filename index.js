const express = require ('express');
const path = require ('path');

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/ririsrecipes', {
    useNewUrlParser : true,
    useUnifiedTopology : true
});

const NewRecipe = mongoose.model('NewRecipe', {
    content: String
});

const AdminDetails = mongoose.model('AdminDetails', {
    email : String,
    password : String
});

const session = require('express-session');

var app = express ();
app.use(express.urlencoded({ extended: true }));

app.use(session({
    secret : "supersecret",
    resave : false,
    saveUninitialized : true
}));

app.set ('views', path.join (__dirname, 'views'));
app.use (express.static(__dirname + '/public'));

app.set ('view engine', 'ejs');

app.get ('/', function (req, res)
{
    res.render('home');
});

app.post('/addrecipe', function (req, res) {
    var content = req.body.content;

        var recipe = {
            content: content
        }

        var newRecipe = new NewRecipe(recipe);

        newRecipe.save().then(function (){
            console.log("Recipe Successfully Added!");
        });
    
        res.redirect('/recipes');

});

app.get('/recipes', function(req,res){
    NewRecipe.find({}).exec(function (err, allRecipes){
        console.log(err);
        console.log(allRecipes, "allRecipes")
       res.render('recipes', {allRecipes : allRecipes});
    });
});

app.get('/allrecipes', function(req,res){
    NewRecipe.find({}).exec(function (err, allRecipes){
        console.log(err);
        console.log(allRecipes, "allRecipes")
       res.render('allrecipes', {allRecipes : allRecipes});
    });
});

app.get('/addrecipe', function(req,res){
    res.render('addrecipe');
});

app.get('/admin', function(req, res){
    res.render('admin');
});


app.post('/admin', function(req,res){
    var getEmail = req.body.email;
    var getPassword = req.body.password;

    AdminDetails.findOne({email : getEmail, password : getPassword}).exec(function(err,admin){
        if (admin) {
            req.session.email = admin.email;
            req.session.userLoggedIn = true;
            res.redirect('/recipes');
        }
        else { 
            res.render('admin', { error : "Incorrect email or password" });
        }
    });

});

app.get('/deleterecipe/:id', function(req,res){
    if (req.session.email) {
        var id = req.params.id;
        console.log(id);
        NewRecipe.findByIdAndDelete({_id: id}).exec(function(err, recipe){
            console.log("Error: " + err);
            console.log("Order: " + recipe);
            if (recipe) {
                res.render('deleterecipe', {message : "Blog post deleted."});
            }
            else {
                res.render('deleterecipe', {message : "Sorry, blog post not found"});
            }
        });
    }
    else {
        res.redirect('/admin');  
    }
});

app.get('/editrecipe/:id', function(req,res){
    if (req.session.email) {
        var id = req.params.id;
        console.log(id);
        NewRecipe.findOne({_id: id}).exec(function(err, recipe){
            console.log("Error: " + err);
            console.log("Order: " + recipe);
            if (recipe) {
                res.render('editrecipe', {recipe : recipe});
            }
            else {
                res.send ('This blog post does not exist');
            }
        });
    }
    else {
        res.redirect('/admin'); 
    }
});

app.post('/editrecipe/:id', function (req, res) {
        var content = req.body.content;
        var id = req.params.id;
        NewRecipe.findOne({id : id}).exec(function(err, recipe){
            recipe.content = content;
            recipe.save();
        });
    
        res.redirect('/recipes');

    }
);

app.listen(8000); 

console.log('Application is running on port 8000');



